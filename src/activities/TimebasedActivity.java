package activities;


/**
 * This class describes an activity whose only criteria for finishing
 * is to achieve a specified running time.
 * 
 * @author Matt
 *
 */
public class TimebasedActivity extends CommonActivity {

	
	public TimebasedActivity(String nameOfActivity, float timeForActivityCompletion) {
		super(nameOfActivity, timeForActivityCompletion);
	}

	@Override
	public boolean hasBeenAchieved() {
		return timeRunning >= timeForCompletion;
	}
	
	
}
