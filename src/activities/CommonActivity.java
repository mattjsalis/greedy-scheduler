package activities;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import Utility.NumberUtility;

import java.util.Map.Entry;

import taskresources.Allocatable;
import taskresources.Exhaustible;
import taskresources.Resource;

/**
 * Common class for activities
 * @author Matt
 *
 */
public abstract class CommonActivity implements Activity{
	
	
	/**
	 * Activities that must be completed before this activity
	 */
	protected Collection<Activity> prerequisiteActivities = new ArrayList<Activity>();
	protected float timeForCompletion;
	protected float timeRunning = 0.f;
	protected String activityName;
	
	
	protected boolean isMandatory = false;
	protected boolean hasBeenScheduled = false;
	protected int priority;
	/**
	 * Maps the name of a resource to the depletion caused per time step by
	 * this activity
	 */
	protected Map<String, Number> resourceRequirementMap = new HashMap<String, Number>();
	
	
	protected CommonActivity(String nameOfActivity, float timeForActivityCompletion){
		activityName = nameOfActivity;
		timeForCompletion = timeForActivityCompletion;
	}
	
	@Override
	public void addResourceRequirement(Resource resource, Number amountRequired){
		
		resourceRequirementMap.put(resource.getNameOfResource(), amountRequired);
	}
	
	/**
	 * Allocates resources required for this activity
	 */
	@Override
	public boolean allocateResources(float simStep, float simTime, Collection<Resource> resources) {
		if(!isAllAllocatableResourcesAvailable(simStep, resources)){return false;}
		for(Entry<String,Number> allocationEntry : resourceRequirementMap.entrySet()){
			Optional<Resource> resource = resources
											.stream()
											.filter(res -> res.getNameOfResource() == allocationEntry.getKey())
											.filter(res -> res instanceof Allocatable)
											.findFirst()
											;
			
			if(resource.isPresent()){
				Allocatable allocatableResource = (Allocatable) resource.get();
				allocatableResource.allocateResource(activityName, simTime, timeForCompletion, allocationEntry.getValue());
			}
		}
		
		return true;
		
	}
	

	@Override
	public boolean depleteResources(float timeStep, Collection<Resource> resources) {
		if(!isAllExhaustibleResourcesAvailable(timeStep, resources)){return false;}
		for(Entry<String,Number> depleteEntry : resourceRequirementMap.entrySet()){
			Optional<Resource> resource = resources
											.stream()
											.filter(res -> res.getNameOfResource() == depleteEntry.getKey())
											.filter(res -> res instanceof Exhaustible)
											.findFirst()
											;
			
			if(resource.isPresent()){
				Number amountToDeplete = NumberUtility.multiply(depleteEntry.getValue(),new Float(timeStep));
				if(resource.get().isEnoughResourceForStart(amountToDeplete)){
					Exhaustible exhaustible = (Exhaustible)resource.get();
					exhaustible.removeResource(amountToDeplete);
					timeRunning += timeStep;
				}
	
			}

		}
		
		return true;
	}
	
	@Override
	public boolean getIsMandatory() {
		return isMandatory;
	}

	

	@Override
	public boolean hasBeenScheduled() {
		return hasBeenScheduled;
	}
	
	@Override
	public void setHasBeenScheduled(boolean hasBeenScheduled){
		this.hasBeenScheduled = hasBeenScheduled;
	}
	
	@Override
	public String getNameOfActivity(){
		return activityName;
	}

	@Override
	public int getPriority() {
		return priority;
	}

	@Override
	public void setPriority(int priority) {
		this.priority = priority;
	}
	

	@Override
	public void setIsMandatory(boolean isMandatory) {
		this.isMandatory = isMandatory;
	}

	@Override
	public boolean isPrequisiteActivitiesFinished() {
		return prerequisiteActivities.stream().allMatch(activity -> activity.hasBeenAchieved());
	}

	@Override
	public void addPrerequisiteActivities(Activity... activities) {
		for(Activity activity : activities)
			prerequisiteActivities.add(activity);
	}

	@Override
	public boolean attemptToSchedule(
			float simStep, 
			float simTime,
			Collection<Resource> resources) {

		if( isPrequisiteActivitiesFinished() 
				&& isAllResourcesAvailable(simStep, resources)
				){
			allocateResources(simStep, simTime, resources);
			hasBeenScheduled = true;
			return true;
		}
		
		return false;
	}

	@Override
	public boolean isAllResourcesAvailable(float simStep, Iterable<Resource> resources) {
		for(Resource resource : resources){
			Number requiredResource = resourceRequirementMap.get(resource.getNameOfResource());
			if(requiredResource != null && resource instanceof Exhaustible){
				requiredResource = NumberUtility.multiply(requiredResource, new Float(simStep));
			}
			if(requiredResource != null){
				if(!resource.isEnoughResourceForStart(requiredResource)){
					return false;
				}
			}
		}

		return true;
	}
	
	public boolean isAllAllocatableResourcesAvailable(float simStep, Collection<Resource> resources){
		Iterable<Resource> allocatable = () -> resources.stream().filter(res -> res instanceof Allocatable).iterator();
		return isAllResourcesAvailable(simStep, allocatable);
	}
	
	public boolean isAllExhaustibleResourcesAvailable(float simStep,Collection<Resource> resources){
		Iterable<Resource> exhaustible = () -> resources.stream().filter(res -> res instanceof Exhaustible).iterator();
		return isAllResourcesAvailable(simStep, exhaustible);
	}
	
	

	@Override
	public void releaseResourceAllocations(float simTime, Collection<Resource> resources){
		Iterable<Resource> allocatableResIter = () -> resources
									.stream()
									.filter(res -> res instanceof Allocatable)
									.iterator();
		for(Resource resource : allocatableResIter){
			((Allocatable)resource).releaseAllocatedResources(this.activityName, simTime);
		}
	}
	
		
	
}
