package activities;

import java.util.Collection;
import taskresources.Resource;
/**
 * This is an activity that can be scheduled
 * @author Matt
 *
 */
public interface Activity {
	
	/**
	 * Is this a mandatory activity?
	 * @return
	 */
	public boolean getIsMandatory();
	
	/**
	 * Sets the flag as mandatory
	 * @param isMandatory
	 */
	public void setIsMandatory(boolean isMandatory);
	
	/**
	 * Has this activity been achieved?
	 * @return
	 */
	public boolean hasBeenAchieved();
	
	public boolean hasBeenScheduled();
	
	public void setHasBeenScheduled(boolean hasBeenScheduled);
	
	/**
	 * Returns the priority of this activity where a lower
	 * value indicates higher priority
	 * @return
	 */
	public int getPriority();
	
	/**Sets the priority of this activity where a lower
	 * value indicates higher priority
	 * @param priority
	 */
	public void setPriority(int priority);
	
	/**
	 * Depletes any exhaustible resources required for this activity
	 * @return boolean indicating if the resources were available to consume
	 */
	public boolean depleteResources(float timeStep, Collection<Resource> resources);
	
	/**
	 * Allocate any allocatable resources for this activity
	 * @param resources
	 * @return boolean indicating if resources were available for allocation
	 */
	public boolean allocateResources(float simStep, float simTime, Collection<Resource> resources);

	public void releaseResourceAllocations(float simTime, Collection<Resource> resources);
	
	/**
	 * Returns a boolean indicating if all of the prerequisite activities have been finished
	 * @return
	 */
	public boolean isPrequisiteActivitiesFinished();
	
	public void addResourceRequirement(Resource resource,  Number amountRequired);
	
	public void addPrerequisiteActivities(Activity... activity);
	
	/**
	 * Attempt to schedule this activity.
	 * @param simTime
	 * @param allocatableResources
	 * @param exhaustibleResources
	 * @return boolean describing if the activity was scheduled
	 */
	public boolean attemptToSchedule(
			float simStep,
			float simTime,
			Collection<Resource> resources);
	
	/**
	 * Returns a boolean indicating if all of the resources have enough
	 * left to be allocated for this activity
	 * @param resources
	 * @return
	 */
	public boolean isAllResourcesAvailable(float simStep, Iterable<Resource> resources);
	
	public String getNameOfActivity();
	
}
