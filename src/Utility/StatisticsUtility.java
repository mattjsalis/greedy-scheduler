package Utility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class StatisticsUtility {
	
	private static Random random = new Random();
	
	public static float getRandomFloatBetween(float lower, float upper){
		return random.nextFloat()*(upper - lower) + lower;
	}
	
	/**
	 * Returns a list of ints between the lower and upper bound that is lower bound inclusive
	 * and upper bound non-inclusive
	 * @param lower
	 * @param upper
	 * @return
	 */
	public static List<Integer> getRandomizedListOfIntsBetween(int lower, int upper){
		List<Integer> ints = new ArrayList<Integer>();
		for(int i = lower; i<upper;i++){
			ints.add(i);
		}
		Collections.shuffle(ints);
		return ints;
	}
}
