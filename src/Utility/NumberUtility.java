package Utility;

public class NumberUtility {
	
	@SuppressWarnings("unchecked")
	public static <T extends Number> T minus(T number, T subtractor){
		if(number instanceof Double){
			return (T) new Double(number.doubleValue() - subtractor.doubleValue());
		} else if (number instanceof Float){
			return (T) new Float(number.floatValue() - subtractor.floatValue());
		} else if (number instanceof Integer){
			return (T) new Integer(number.intValue() - subtractor.intValue());
		}
		
		throw new UnsupportedOperationException( "Number type is not supported in NumberUtility.");
	}
	
	
	@SuppressWarnings("unchecked")
	public static <T extends Number> T sum(T number, T addedValue){
		if(number instanceof Double){
			return (T) new Double(number.doubleValue() + addedValue.doubleValue());
		} else if (number instanceof Float){
			return (T) new Float(number.floatValue() + addedValue.floatValue());
		} else if (number instanceof Integer){
			return (T) new Integer(number.intValue() + addedValue.intValue());
		}
		
		throw new UnsupportedOperationException( "Number type is not supported in NumberUtility.");
	}
	
	@SuppressWarnings("unchecked")
	public static <T extends Number> T multiply(T number, Number multiplier){
		if(number instanceof Double){
			return (T) new Double(number.doubleValue() * multiplier.doubleValue());
		} else if(number instanceof Float){
			return (T) new Float(number.floatValue() * multiplier.floatValue());
		}
		
		throw new UnsupportedOperationException( "Number type is not supported in NumberUtility.");
	}
	
	public static <T extends Number> boolean greaterThan(T firstNumber, T secondNumber){
		return firstNumber.doubleValue() > secondNumber.doubleValue();
	}
	
	public static <T extends Number> boolean lessThan(T firstNumber, T secondNumber){
		return firstNumber.doubleValue() < secondNumber.doubleValue();
	}
	
	public static <T extends Number> boolean greaterThanOrEqualTo(T firstNumber, T secondNumber){
		return firstNumber.doubleValue() >= secondNumber.doubleValue();
	}
	
	public static <T extends Number> boolean lessThanOrEqualTo(T firstNumber, T secondNumber){
		return firstNumber.doubleValue() <= secondNumber.doubleValue();
	}
	@SuppressWarnings("unchecked")
	public static <T extends Number> T zero(T number){
		if(number instanceof Double){
			return (T) new Double(0.0);
		} else if (number instanceof Float){
			return (T) new Float(0.0);
		} else if (number instanceof Integer){
			return (T) new Integer(0);
		}
		
		throw new UnsupportedOperationException( "Number type is not supported in NumberUtility.");
	}
}
