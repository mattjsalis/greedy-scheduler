package simexecution;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import Utility.NumberUtility;
import Utility.StatisticsUtility;
import activities.Activity;
import activities.TimebasedActivity;
import scheduler.ActivityScheduler;
import scheduler.NonBackTrackingScheduler;
import taskresources.BatteryEnergy;
import taskresources.CommunicationLinks;
import taskresources.Exhaustible;
import taskresources.Propellant;
import taskresources.Resource;
import taskresources.ResourceReplenisher;
import taskresources.SimulatedRam;
import taskresources.SolarPanel;

/**
 * This class serves to run a simulation of the activity scheduling sequence.
 * @author Matt
 *
 */
public class ActivityScheduleSimulation {
	
	private float simTime = 0.f;
	private float simStep = 0.001f;
	private float totalTime = 1000.f;
	
	
	private Float intialBatteryEnergy = 10.f;
	private Float maxBatteryEnergy = 10.f;
	
	private Float totalRam = 8.0f;
	
	private Integer totalCommsLinks = 5;
	
	private Float maxPropellant = 1000.f;
	private Float initialPropellant = 50.f;
	private Float timeToCompletePropellantBurn = 1.0f;
	
	private Float rateOfSolarPanelEnergyConversion = 1000.f;
	
	private Float rateOfPropellantConsumption = 50.f;
	private Float rateOfBatteryConsumptionPropellantActivity = 1000.f;
	private Float requiredRamForPropellantActivity = 1.f;
	
	private Float timeToCompleteMessageActivity = 0.5f;
	private Integer requiredCommLinksForMessage = 1;
	private Float rateOfBatteryConsumptionMessageActivity = 10000.f;
	private Float requiredRamForMessageActivity = 0.5f;
	
	private Float timeToCompleteImagingTask = 0.25f;
	private Float requiredRamForImagingTask = 4.f;
	private Float rateOfBatteryConsumptionForImagingTask = 10000.f;
	
	
	private Collection<Activity> activitiesToSchedule = new ArrayList<Activity>();
	private Collection<Resource> resources = new ArrayList<Resource>();
	private Collection<ResourceReplenisher> resourceReplensishers = new ArrayList<ResourceReplenisher>();
	private ActivityScheduler scheduler = new NonBackTrackingScheduler();
	
	public ActivityScheduleSimulation(){
		defineSemiStochasticScenario();
		randomlyAssignPriorities();
	}
	
	public float simulateScheduling(){
		
		while(simTime < totalTime
				&& !activitiesToSchedule.stream().allMatch(a -> a.hasBeenAchieved())){
			
			simulateASchedulingStep();
			printProgress();
			simTime += simStep;
		}
		printScheduleResults();
		return simTime;
	}
	
	private void printProgress(){
		StringBuilder sb = new StringBuilder();
		sb.append("Completed ").append(scheduler.getNumberCompletedActivities())
			.append(" of ")
			.append(activitiesToSchedule.size());
		System.out.println(sb);
	}
	
	private long getRunnningTasks(){
		return activitiesToSchedule.stream().filter(a -> a.hasBeenScheduled() && !a.hasBeenAchieved()).count();
	}
	
	private void printExhaustedResources(){
		String resourceMessage = resources.stream()
				
				/*.filter(res -> 
					res instanceof Exhaustible
					&& NumberUtility.lessThanOrEqualTo(
							res.getAmountOfAvailableResource(),
							NumberUtility.zero(res.getAmountOfAvailableResource())
						)
					)*/
				.map(res -> 
					res.getNameOfResource() + ": " 
					+ res.getAmountOfAvailableResource().toString()
				)
				.collect(Collectors.joining(" | "));
			if(!resourceMessage.isEmpty()){
				System.out.println(resourceMessage);
			}
	}
	public void printPriorities(){
		String priorityStr = activitiesToSchedule.stream()
			.map(a -> a.getNameOfActivity() + ": " + new Integer(a.getPriority()).toString())
			.collect(Collectors.joining(System.getProperty("line.separator")));
		System.out.println(priorityStr);
		
	}
	
	private void simulateASchedulingStep(){
		scheduler.scheduleActivities(simStep, simTime, resources, activitiesToSchedule);
		scheduler.depleteResourcesForScheduledActivities(simStep, resources);
		scheduler.removeCompletedActivities(simTime, resources);
		for(ResourceReplenisher replenisher : resourceReplensishers){
			replenisher.replenishResource(simStep);
			
		}
		
	}
	
	public void printScheduleResults(){
		StringBuilder sb = new StringBuilder();
		sb.append("Total activites: ").append(activitiesToSchedule.size());
		sb.append(System.getProperty("line.separator"));
		sb.append("Scheduled activites: ").append(activitiesToSchedule.stream().filter(a -> a.hasBeenScheduled()).count());
		sb.append(System.getProperty("line.separator"));
		sb.append("Completed activites: ").append(scheduler.getNumberCompletedActivities());
		System.out.println(sb);
	}
	
	private void randomlyAssignPriorities(String... activitiesTonotAssignRandomly){
		List<Integer> randomPriorityAssignments = 
				StatisticsUtility.getRandomizedListOfIntsBetween(0, activitiesToSchedule.size() - activitiesTonotAssignRandomly.length);
		Iterator<Integer> intIterator = randomPriorityAssignments.iterator();
		for(Activity activity : activitiesToSchedule){
			if(Arrays.asList(activitiesTonotAssignRandomly)
					.contains(activity.getNameOfActivity())){
				continue;
			}
			activity.setPriority(intIterator.next());
		}
	}

	/**
	 * Method to define a somewhat stochastic scenario
	 */
	private void defineSemiStochasticScenario(){
		//Define some allocatable resources
		SimulatedRam simulatedRam = new SimulatedRam("RAM");
		simulatedRam.setTotalAvailableResourceAmount(totalRam);
		CommunicationLinks commsLinks = new CommunicationLinks("Comms Links");
		commsLinks.setTotalAvailableResourceAmount(totalCommsLinks);
		
		//Define some exhaustible resources
		BatteryEnergy batteryEnergy = new BatteryEnergy("Battery energy");
		batteryEnergy.setInitialResourceAmount(intialBatteryEnergy);
		batteryEnergy.setTotalAvailableResourceAmount(maxBatteryEnergy);
		Propellant propellant = new Propellant("Propellant");
		propellant.setInitialResourceAmount(initialPropellant);
		propellant.setTotalAvailableResourceAmount(maxPropellant);
		
		
		resources.add(propellant);
		resources.add(batteryEnergy);
		resources.add(commsLinks);
		resources.add(simulatedRam);
		
		//Define a solar panel to replenish the battery energy
		SolarPanel solarPanel = new SolarPanel(batteryEnergy, rateOfSolarPanelEnergyConversion);
		resourceReplensishers.add(solarPanel);
		
		//Define some activites to schedule
		Activity attitudeAdjustment = new TimebasedActivity("Adjust attitude for scan", timeToCompletePropellantBurn);
		attitudeAdjustment.setIsMandatory(true);
		attitudeAdjustment.setPriority(0);
		attitudeAdjustment.addResourceRequirement(propellant, rateOfPropellantConsumption);
		attitudeAdjustment.addResourceRequirement(batteryEnergy, rateOfBatteryConsumptionPropellantActivity);
		attitudeAdjustment.addResourceRequirement(simulatedRam, requiredRamForPropellantActivity);
		
		
		activitiesToSchedule.add(attitudeAdjustment);
		
		//Define a bunch of non mandatory messaging activities that require a 
		// random amount of time between 0.02 and 0.10 time units
		for (Integer i_msg = 0; i_msg < 10 ; i_msg++){
			Activity newMessageActivity = new TimebasedActivity("Non mandatory Message " + i_msg.toString(),
					timeToCompleteMessageActivity);
			newMessageActivity.setIsMandatory(false);
			newMessageActivity.addResourceRequirement(commsLinks, requiredCommLinksForMessage);
			newMessageActivity.addResourceRequirement(batteryEnergy, rateOfBatteryConsumptionMessageActivity);
			newMessageActivity.addResourceRequirement(simulatedRam, requiredRamForMessageActivity);
			activitiesToSchedule.add(newMessageActivity);
		}
		//Define a bunch of mandatory messaging activities that require a 
		// random amount of time between 0.02 and 0.10 time units
		// that are dependent on the attitude adjustment
		for (Integer i_msg = 0; i_msg < 10 ; i_msg++){
			Activity newMessageActivity = new TimebasedActivity("Other Message " + i_msg.toString(),
					timeToCompleteMessageActivity);
			newMessageActivity.setIsMandatory(true);
			newMessageActivity.addResourceRequirement(commsLinks, requiredCommLinksForMessage);
			newMessageActivity.addResourceRequirement(batteryEnergy, rateOfBatteryConsumptionMessageActivity);
			newMessageActivity.addResourceRequirement(simulatedRam, requiredRamForMessageActivity);
			//newMessageActivity.addPrerequisiteActivities(attitudeAdjustment);
			activitiesToSchedule.add(newMessageActivity);
		}
		
		//Define a bunch of mandatory imaging that depend on the attitude adjustment
		// and some amount of the previously defined mandatory messages
		for (Integer i_img = 0; i_img < 10; i_img++){
			Activity newImageActivity = new TimebasedActivity("Image " + i_img.toString(), timeToCompleteImagingTask);
			newImageActivity.setIsMandatory(true);
			newImageActivity.addResourceRequirement(batteryEnergy, rateOfBatteryConsumptionForImagingTask);
			newImageActivity.addResourceRequirement(simulatedRam, requiredRamForImagingTask);
			//newImageActivity.addPrerequisiteActivities(attitudeAdjustment);
			//Add the required messages as prerequisitites
			activitiesToSchedule.add(newImageActivity);
			//Add a message that relies on this particular image being created
			Activity newMessage = new TimebasedActivity("Image message" + i_img.toString(), this.timeToCompleteMessageActivity);
			newMessage.addPrerequisiteActivities(newImageActivity);
			newMessage.addResourceRequirement(commsLinks, requiredCommLinksForMessage);
			newMessage.addResourceRequirement(batteryEnergy, rateOfBatteryConsumptionMessageActivity);
			newMessage.addResourceRequirement(simulatedRam, requiredRamForMessageActivity);
			activitiesToSchedule.add(newMessage);
		}
	}
}
