package simexecution;

public class TestMain {
	
	
	public static void main(String[] args){
		ActivityScheduleSimulation sim = new ActivityScheduleSimulation();
		float completionTime = sim.simulateScheduling();
		//sim.printPriorities();
		System.out.println("Sim exited at time: " + new Float(completionTime).toString());
		System.out.println("");
	}
	
}
