package taskresources;

import java.util.ArrayList;
import java.util.Collection;

import Utility.NumberUtility;

import static Utility.NumberUtility.*;

/**
 * Provides common method definitions for an allocatable resource.
 * @author Matt
 *
 */
public abstract class CommonAllocatableResource implements Resource, Allocatable {
	
	/**
	 * Name of the resource given by the user
	 */
	protected String resourceName = null;
	
	/**
	 * Total amount of the resource that may be allocated
	 */
	protected Number maxAmountOfResource;
	
	/**
	 * All of the current resource allocations
	 */
	protected Collection<ResourceAllocation> resourceAllocations = new ArrayList<ResourceAllocation>();
	
	
	protected CommonAllocatableResource(String nameOfResource){
		resourceName = nameOfResource;
	}
	
	
	@Override
	public boolean isEnoughResourceForStart(Number resourceToStart) {
		return greaterThanOrEqualTo(getAmountOfAvailableResource(), resourceToStart);
	}
	@Override
	public Number getAmountOfAvailableResource(){
		return minus(maxAmountOfResource, resourceAllocations
				.stream()
				.map(resAllo -> resAllo.getAmountAllocated())
				.reduce(zero(maxAmountOfResource), NumberUtility::sum));
	}
	@Override
	public void allocateResource(
			String nameOfActivity,
			float timeOfAllocation,
			float timeSpanOfAllocation,
			Number amountToAllocate){
		//System.out.println("Allocating: " + amountToAllocate.toString() + " of " + resourceName + " to " + nameOfActivity);
		if(!isEnoughResourceForStart(amountToAllocate)){return;}
		resourceAllocations.add(new ResourceAllocation(
				nameOfActivity,
				amountToAllocate));
	}
	
	@Override
	public void releaseAllocatedResources(final String nameOfActivity, final float simTime){
		Number wasAvailable = getAmountOfAvailableResource();
		
		resourceAllocations.removeIf(resAlloc-> resAlloc.isNameMatchToActivity(nameOfActivity));
		/**
		System.out.println("Releasing " + resourceName + " for " + nameOfActivity 
				+ ". Was " + wasAvailable.toString()
				+ ". Is " + getAmountOfAvailableResource())
				;**/
				
	}
	
	@Override
	public String getNameOfResource(){
		return resourceName;
	}
	
	/**
	 * Sets the amount of resource available
	 * @param amountOfResource
	 */
	@Override
	public void setInitialResourceAmount(Number amountOfResource){
		
	}
	
	/**
	 * Sets the amount of resource available
	 * @param amountOfResource
	 */
	@Override
	public void setTotalAvailableResourceAmount(Number amountOfResource){
		maxAmountOfResource = amountOfResource;
	}
	

}
