package taskresources;

/**
 * Class to hold the time of the expiration of the allocation
 * and the value of the allocation
 * @author Matt
 *
 */
public class ResourceAllocation{
	private String nameOfActivity;
	private Number amountAllocated;
	
	public ResourceAllocation(
			String nameOfActivity,
			Number amountAllocated){
		this.amountAllocated = amountAllocated;
		this.nameOfActivity = nameOfActivity;
	}
	
	
	public boolean isNameMatchToActivity(String activityName){
		return activityName == nameOfActivity;
	}
	
	public Number getAmountAllocated(){return amountAllocated;}
}