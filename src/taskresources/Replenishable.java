package taskresources;

/**
 * Interface to indicate that a resource is replenishable
 * @author Matt
 *
 */
public interface Replenishable {
	/**
	 * Method that replenishes some portion of the resource
	 */
	public void replenishResource(Number amountToReplenish);
}
