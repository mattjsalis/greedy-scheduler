package taskresources;

public interface Allocatable {
	
	/**
	 * Sets an amount of resource to be allocated for a given amount of time
	 * @param amountToAllocate
	 * @param timeToAllocate
	 */
	public void allocateResource(
			String nameOfActivity,
			float timeOfAllocation,
			float timeSpanOfAllocation,
			Number amountAllocated);
	/**
	 * Releases any allocated resources as a function of time
	 * @param simTime
	 */
	public void releaseAllocatedResources(String nameOfActivity, float simTime);

}
