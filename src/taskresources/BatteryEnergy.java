package taskresources;

import static Utility.NumberUtility.*;

public class BatteryEnergy extends CommonExhaustibleResource implements Replenishable{

	public BatteryEnergy(String nameOfResource) {
		super(nameOfResource);
	}

	@Override
	public void replenishResource(Number energyToReplenish) {
		Number newEnergy = sum(currentAmountOfResource ,energyToReplenish);
		currentAmountOfResource = lessThanOrEqualTo(newEnergy , maximumAmountOfResource)?
				newEnergy : maximumAmountOfResource;
	}
	
}
