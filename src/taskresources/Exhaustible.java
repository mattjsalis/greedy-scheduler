package taskresources;

/**
 * This class defines a resource that can be exhausted such as energy.
 * @author Matt
 *
 */
public interface Exhaustible{
	
	/**
	 * Method returning if this resource has been exhausted
	 * @return
	 */
	public boolean isExhausted();
	
	public void removeResource(Number resourceAmount);
	
}
