package taskresources;

public class SolarPanel implements ResourceReplenisher{
	
	private BatteryEnergy batteryEnergy;
	private float rateOfEnergyReplenishment;
	
	public SolarPanel(BatteryEnergy batteryEnergy,
			float rateOfEnergyReplenishment){
		this.batteryEnergy = batteryEnergy;
		this.rateOfEnergyReplenishment = rateOfEnergyReplenishment;
	}
	
	@Override
	public void replenishResource(float timeInterval) {
		batteryEnergy.replenishResource(rateOfEnergyReplenishment*timeInterval);
	}

}
