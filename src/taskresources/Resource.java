package taskresources;

/**
 * Interface for any resource
 * @author Matt
 *
 */
public interface Resource {
	
	public boolean isEnoughResourceForStart(Number resourceToStart);
	
	public Number getAmountOfAvailableResource();
	
	public String getNameOfResource();

	/**
	 * Sets the amount of resource available
	 * @param amountOfResource
	 */
	public void setInitialResourceAmount(Number amountOfResource);
	
	/**
	 * Sets the amount of resource available
	 * @param amountOfResource
	 */
	public void setTotalAvailableResourceAmount(Number amountOfResource);
	

}
