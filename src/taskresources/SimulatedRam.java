package taskresources;

/**
 * Represents random access memory of a computational unit
 * @author Matt
 *
 */
public class SimulatedRam extends CommonAllocatableResource{
	
	public SimulatedRam(String nameOfResource){
		super(nameOfResource);
	}
}
