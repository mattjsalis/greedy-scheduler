package taskresources;

/**
 * Represents a simulation of a set amount of available links for communication
 * @author Matt
 *
 */
public class CommunicationLinks extends CommonAllocatableResource{

	public CommunicationLinks(String nameOfResource) {
		super(nameOfResource);
	}

}
