package taskresources;

/**
 * Interface to define required methods for a class that replenishes an exhaustible resource.
 * @author Matt
 *
 */
public interface ResourceReplenisher {
	/**
	 * Method to replenish a certain amount of a resource given
	 * a interval of time
	 * @param timeInterval
	 */
	public void replenishResource(float timeInterval);
}
