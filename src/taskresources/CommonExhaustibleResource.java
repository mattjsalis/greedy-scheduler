package taskresources;

import static Utility.NumberUtility.*;

public abstract class CommonExhaustibleResource implements Resource, Exhaustible{

	protected String nameOfResource;
	protected Number maximumAmountOfResource;
	protected Number currentAmountOfResource;
	
	protected CommonExhaustibleResource(String nameOfResource){
		this.nameOfResource = nameOfResource;
	}
	
	@Override
	public boolean isExhausted() {
		
		return currentAmountOfResource.doubleValue() > 0.0;
	}
	
	@Override
	public boolean isEnoughResourceForStart(Number resourceToStart) {
		
		return greaterThanOrEqualTo(currentAmountOfResource, resourceToStart);
	}

	@Override
	public void removeResource(Number resourceAmount) {
		Number newAmount = minus(currentAmountOfResource, resourceAmount);
		currentAmountOfResource = newAmount.doubleValue() >= 0.0 ? 
				newAmount
				:
				zero(newAmount);
			
	}


	@Override
	public Number getAmountOfAvailableResource() {
		return currentAmountOfResource;
	}

	@Override
	public String getNameOfResource() {
		return nameOfResource;
	}

	@Override
	public void setInitialResourceAmount(Number amountOfResource) {
		currentAmountOfResource = amountOfResource;
	}

	@Override
	public void setTotalAvailableResourceAmount(Number amountOfResource) {
		
		maximumAmountOfResource = amountOfResource;
	}

}
