package scheduler;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;

import activities.Activity;
import taskresources.Resource;

/**
 * This class serves to schedule activities in a non-backtracking and greedy way.
 * Activities are sorted by priority and scheduled if there are sufficient resources. Activities
 * of lower priority may be scheduled ahead of higher priority activities in the case that resources
 * are unavailable for higher priority activities.
 * @author Matt
 *
 */
public class NonBackTrackingScheduler implements ActivityScheduler{

	
	protected Collection<Activity> scheduledActivites = new ArrayList<Activity>();
	protected Collection<Activity> completedActivites = new ArrayList<Activity>();
	protected Comparator<Activity> sortByPriority = (activity1, activity2)->Integer.compare(activity1.getPriority(), activity2.getPriority());
	
	@Override
	public void scheduleActivities(
			final float simStep,
			final float simTime,
			Collection<Resource> resources,
			Collection<Activity> activitiesToSchedule) {
		
		//Sort activites by priority
		Iterable<Activity> activityStream = ()->activitiesToSchedule
				.stream()
				.filter(activity -> !activity.hasBeenScheduled()) //only schedule unscheduled activites
				.sorted(sortByPriority)
				.iterator();
		//Attempt to schedule activities by priority
		for(Activity activity : activityStream){
			boolean isScheduled = activity.attemptToSchedule(simStep, simTime, resources);
			if(isScheduled){
				scheduledActivites.add(activity);
				activity.setHasBeenScheduled(true);
			}
		}
		
	}

	@Override
	public void removeCompletedActivities(float simTime, Collection<Resource> resources) {
		for(Activity activity : scheduledActivites){
			if(activity.hasBeenAchieved()){
				completedActivites.add(activity);
				
				activity.releaseResourceAllocations(simTime, resources);
			}
		}
		scheduledActivites.removeIf(activity -> activity.hasBeenAchieved());
	}

	@Override
	public int getNumberScheduledActivities() {
		return scheduledActivites.size();
	}

	@Override
	public int getNumberCompletedActivities() {
		// TODO Auto-generated method stub
		return completedActivites.size();
	}

	@Override
	public void depleteResourcesForScheduledActivities(float timeStep, Collection<Resource> resources) {
		for(Activity activity : scheduledActivites){
			activity.depleteResources(timeStep, resources);
		}
		
	}
	
	
	
	
}
