package scheduler;

import java.util.Collection;

import activities.Activity;
import taskresources.Resource;

public interface ActivityScheduler {
	
	public void depleteResourcesForScheduledActivities(float timeStep, Collection<Resource> resources);
	
	public void scheduleActivities(
			final float simStep,
			final float simTime, 
			Collection<Resource> resources,
			Collection<Activity> activitiesToSchedule);
	
	public void removeCompletedActivities(float simTime, Collection<Resource> resources);
	
	public int getNumberScheduledActivities();
	
	public int getNumberCompletedActivities();
	
}
